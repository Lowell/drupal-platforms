<div id="block-<?php print $block->module .'-'. $block->delta ?>" class="<?php print $block_classes . ' ' . $block_zebra; ?>">
  <div class="block-inner">

    <?php if (!empty($block->subject)): ?>
      <h3 class="title block-title"><?php print $block->subject; ?></h3>
    <?php endif; ?>

    <div class="content">
		<!--subscriber login -->
		<div id="subscribe_button">
			<?php
			global $user;
			if($user->uid)
			{
			?>
			<a href="<?php global $base_url;	print $base_url.'/'; ?>logout" onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('subscribe','','/<?php print path_to_theme();?>/images/subscribers-logout_mo.gif',1)">
				<img id="subscribe"	src="/<?php print path_to_theme();?>/images/subscribers_logout.gif" border="0">
			</a>
			<?php
			}
			else
			{
			?>
			<a href="<?php global $base_url;	print $base_url.'/'; ?>user/login" onmouseout="MM_swapImgRestore()"
				onmouseover="MM_swapImage('subscribe','','/<?php print path_to_theme();?>/images/subscribe_login_mo.png',1)">
				<img id="subscribe"	src="/<?php print path_to_theme();?>/images/subscribers_login.png" border="0">
			</a>
			<?php
			}
			?>
		</div>
    </div>

    <?php print $edit_links; ?>

  </div> <!-- /block-inner -->
</div> <!-- /block -->